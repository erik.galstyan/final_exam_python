from datetime import datetime as dt

class Patient:

    def __init__(self,name,surname,age,gender):
        self.name = name
        self.surname = surname
        self.age = age
        self.gender = gender

    def __repr__(self):
        return f"{self.name} {self.surname} - {self.gender},{self.age} years old."



class Doctor:

    def __init__(self,name,surname):
        self.name = name
        self.surname = surname
        self.schedule = {}

    
    def __repr__(self):
        
        x = "\n"
        for k,v in self.schedule.items():
            x += f"{k} : {v}\n"

        return f"Doctor {self.name} {self.surname} schedule {x}"


    def register_patient(self,patient,datetime):
        self.schedule[datetime] = patient
        print(f"Patient {patient} successfully registered at {datetime}")

    
    def is_registered(self,patient):
        for k,v in self.schedule.items():
            if v == patient:
                return True
        
        return False


man = Patient("John","Anderson",34,"M")
boy = Patient("David","Backham",3,'M')
woman = Patient("Suzan","Lane",43,"F")
girl = Patient("Eva","Hockings",11,"F")

# print(man)
# print(girl)

doctor = Doctor("Hovhannes","Chillingaryan")
print(doctor)

man_time = dt(2022,10,9,14,25).isoformat()
doctor.register_patient(man,man_time)

print(doctor.is_registered(woman))

boy_time = dt(2022,10,8,15,10).isoformat()
doctor.register_patient(boy,boy_time)

woman_time = dt(2022,10,8,18,50).isoformat()
doctor.register_patient(woman,woman_time)

girl_time = dt(2022,10,10,10,00).isoformat()
doctor.register_patient(girl,girl_time)

print(doctor)