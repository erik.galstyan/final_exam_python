class Product:
    
    def __init__(self,price,id,quantity):
        self.price = price
        self.id = id
        self.quantity = quantity

    def __repr__(self):
        return f"{self.id}'s quantity is {self.quantity},each of which costs {self.price}$"

    def buy(self,count):
        if count > self.quantity or count < 0 :
            raise ValueError("There's NO such product")
        
        self.quantity -= count
        return self.quantity

    def __eq__(self,object):
        if self.price == object.price and self.id == object.id and self.quantity == object.quantity :
            return True
        else:
            return False



apple = Product(400,1,10)
print("Apple -->",apple) # repr
print(apple.buy(6))
#print(apple.buy(7))  #throws exception
apple_2 = Product(400,1,10)
print("Apple_2 -->",apple_2)
print(apple == apple_2) # false
print(apple_2.buy(1))
print(apple_2.buy(3))
print(apple_2.buy(2))
print(apple == apple_2) # true